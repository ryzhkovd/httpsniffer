/*
 * PacketParser.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: root
 */

#include "PacketParser.h"

PacketParser::PacketParser(std::string data)
{
this->_data=data;
}
PacketParser::PacketParser(char* playload, size_t len){
	this->_data=std::string(playload,len);

}
PacketParser::~PacketParser() {

}
std::string PacketParser::getHost()
{
if(this->_host.length()>0)
		return _host;
return "";

}

bool PacketParser::parser() {
	int i = 0;

	std::size_t pos1, pos2;

		pos1 = this->_data.find("Host");
		if (pos1 != std::string::npos)
		{
			pos2 = this->_data.find("\r\n",pos1);
			if (pos2 != std::string::npos)
			{
				i = pos2 - pos1;
				pos1=this->_data.find(" ",pos1)+1;
				this->_host=this->_data.substr(pos1, i-6);
				return true;
			}


		}
return false;

}


/*
 * TcpServer.h
 *
 *  Created on: 15 дек. 2013 г.
 *      Author: root
 */


#ifndef _TCPSERVER_H_
#define _TCPSERVER_H_

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <iostream>
#include "HostStorage.h"
#include <vector>
#include <unistd.h>

extern pthread_mutex_t storageMutex;
extern HostStorage* data;
/// buffer size in bytes
#define BUF_SIZE            1024
/// pending connections to hold
#define CountConnection     100

// server port
#define PORT_NUM 42242
///Client thread
void *ClientThread(void* atr);
class TcpServer {
public:
	TcpServer();
	virtual ~TcpServer();
	bool 	InitSocket();
	bool	Start();
	static std::string FormatXML(std::vector<URLItem> i);
private:
/// Server socket descriptor
unsigned int	_ServerS;
/// Server Internet address
 sockaddr_in	_ServerAddres;
/// Client socket descriptor
unsigned int	_ClientS;
/// Client Internet address
 sockaddr_in	_ClientAddr;
/// Client IP address
 in_addr		_ClientIpAddr;
/// Internet address length
unsigned int	_AddrLen;
/// holds thread args
unsigned int	_Ids;
///  pthread attributes
pthread_attr_t	_Attr;
/// Thread ID (used by OS)
pthread_t		_Threads;

};

#endif /* _TCPSERVER_H_ */

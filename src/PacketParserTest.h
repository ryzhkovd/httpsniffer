/*
 * PacketParserTest.h
 *
 *  Created on: Dec 9, 2013
 *      Author: root
 */

#ifndef PACKETPARSERTEST_H_
#define PACKETPARSERTEST_H_

#include <gtest/gtest.h>
#include "PacketParser.h"
#include "PCabFilter.h"

//class PacketParserTest: public testing::Test {
//protected:
//	virtual void SetUp(){
//		PCabFilter f(1000,"eth0");
//		f.initSniff();
//		f.setFilter("tcp.dstport==80 and ip.src_host==192.168.1.10");
//		//f.startSniff(got_packet);
//
//	}
//
//
//public:
//	PacketParserTest();
//	virtual ~PacketParserTest();
//};

/*

// When you have a test fixture, you define a test using TEST_F
// instead of TEST.

// Tests the default c'tor.
TEST_F(QueueTest, DefaultConstructor) {
  // You can access data in the test fixture here.
  EXPECT_EQ(0u, q0_.Size());
}

// Tests Dequeue().
TEST_F(QueueTest, Dequeue) {
  int * n = q0_.Dequeue();
  EXPECT_TRUE(n == NULL);

  n = q1_.Dequeue();
  ASSERT_TRUE(n != NULL);
  EXPECT_EQ(1, *n);
  EXPECT_EQ(0u, q1_.Size());
  delete n;

  n = q2_.Dequeue();
  ASSERT_TRUE(n != NULL);
  EXPECT_EQ(2, *n);
  EXPECT_EQ(1u, q2_.Size());
  delete n;
}

// Tests the Queue::Map() function.
TEST_F(QueueTest, Map) {
  MapTester(&q0_);
  MapTester(&q1_);
  MapTester(&q2_);
}


 */


#endif /* PACKETPARSERTEST_H_ */

/*
 * logger.h
 *
 *  Created on: 13 дек. 2013 г.
 *      Author: root
 */
#include <string>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#ifndef LOGGER_H_
#define LOGGER_H_

class logger {
public:
	logger();
	virtual ~logger();
	void add(std::string);
private:
	std::ofstream f;
};

#endif /* LOGGER_H_ */

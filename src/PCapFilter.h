/*
 * PCabFilter.h
 *
 *  Created on: Dec 5, 2013
 *      Author: dima
 */

#ifndef PCABFILTER_H_
#define PCABFILTER_H_
#include <iostream>
#include <pcap.h>

#include <stdlib.h>
#include <string>
#include <queue>
#include "headers.h"
#include "PacketParser.h"
#include "HostStorage.h"
#include "logger.h"

extern pthread_mutex_t storageMutex;
extern logger l;
void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);//! callbask



/*!
pcap warper
 */
class PCabFilter {
public:
	PCabFilter(	int timeDelta/**< [in] timeout */,
				char* device/**< [in] dev */
				);
	virtual ~PCabFilter();
	bool initSniff();
	void deativateSniff();//!
	bool setFilter(char*  filter);
	bool startSniff(pcap_handler callback,void* ptr);
private:
	int delta;
	pcap_t *handle;
	char *dev;
	char errbuf[PCAP_ERRBUF_SIZE];
	bool isActive;
	bpf_program compiledFilter;
};

#endif /* PCABFILTER_H_ */

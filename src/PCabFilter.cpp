/*
 * PCabFilter.cpp
 *
 *  Created on: Dec 5, 2013
 *      Author: dima
 */

#include "PCapFilter.h"


PCabFilter::PCabFilter(int timeDelta,char* device)
{
	this->delta=timeDelta;
	this->dev=device;
	isActive =false;
	this->handle=NULL;

}

PCabFilter::~PCabFilter() {
	// TODO Auto-generated destructor stub
	if(isActive==true)
		pcap_close(this->handle);
}
//#define offline
bool PCabFilter::initSniff()
{
#if defined(offline)
	this->handle = pcap_open_offline("./test.pcap",this->errbuf);
#else
	this->handle = pcap_open_live(this->dev, BUFSIZ, 0, this->delta, this->errbuf);
#endif
	if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", this->dev, this->errbuf);
		return false;
	}
	this->isActive=true;
	return true;
}

void PCabFilter::deativateSniff()
{
	if(isActive==true)
		pcap_close(this->handle);
	this->isActive=false;

}

bool PCabFilter::setFilter(char*  filter)
{
	if (pcap_compile(this->handle, &(this->compiledFilter), filter, 0, 0) == -1) {
			 //fprintf(stderr, "Couldn't parse filter");
			 return false;
		 }
	 if (pcap_setfilter(this->handle, &(this->compiledFilter)) == -1) {

			 return false;
		 }
	 return true;
}



void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
	 sniff_ethernet *ethernet;  /* The ethernet header [1] */
	 sniff_ip *ip;              /* The IP header */
	 sniff_tcp *tcp;            /* The TCP header */
	 char *payload;             /* Packet payload */

	 int size_ip;
	 int size_tcp;
	 int size_payload;
	 HostStorage* pt;
if(args!=NULL)
	pt=reinterpret_cast<HostStorage*> (args);
	if(pt!=NULL)
	{
		ethernet = (struct sniff_ethernet*)(packet);
		ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
	 		size_ip = IP_HL(ip)*4;
	 		if (size_ip < 20) {
	 		//	printf("   * Invalid IP header length: %u bytes\n", size_ip);
	 			return;
	 		}
	 		tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
	 		size_tcp = TH_OFF(tcp)*4;
	 		if (size_tcp < 20) {
	 			//printf("   * Invalid TCP header length: %u bytes\n", size_tcp);
	 			return;
	 		}
	 		payload = (char*)(packet + SIZE_ETHERNET + size_ip + size_tcp);
	 		size_payload = ntohs(ip->ip_len) - (size_ip + size_tcp);
	 		if(size_payload>0){
	 			PacketParser pp(std::string(payload,size_payload));
	 			if(pp.parser()){
	 				pthread_mutex_lock(&storageMutex);
	 				pt->addItem(pp.getHost());
	 				pthread_mutex_unlock(&storageMutex);
	 				l.add(pp.getHost());
	 			}
	 		}//packetParser(size_payload, payload);
	 }
}

bool PCabFilter::startSniff(pcap_handler callback, void* ptr)
{

	pcap_loop(this->handle,-1,callback,(u_char*)ptr);
			//pcap_loop(handle, num_packets, got_packet, NULL);
return true;

}


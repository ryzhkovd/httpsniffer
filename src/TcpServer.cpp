/*
 * TcpServer.cpp
 *
 *  Created on: 15 дек. 2013 г.
 *      Author: root
 */

#include <TcpServer.h>

bool TcpServer::InitSocket() {
	_ServerS = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	_ServerAddres.sin_family = PF_INET;
	_ServerAddres.sin_port = htons(PORT_NUM);
	_ServerAddres.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(_ServerS, (struct sockaddr *) &_ServerAddres, sizeof(sockaddr_in)) == -1)
		return false;
	if(listen(_ServerS,CountConnection)==-1)
		return false;
	return true;
}

bool TcpServer::Start()
{
while(true)
{
	_AddrLen = sizeof(_ClientAddr);
	_ClientS = accept(_ServerS, (struct sockaddr *)&_ClientAddr, &_AddrLen);
	if(_ClientS)
	{
		pthread_create (&_Threads,NULL,ClientThread,&_ClientS);
	}
}
return true;
}
std::string TcpServer::FormatXML(std::vector<URLItem> i)
{
std::string str;
str.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
str.append("<Events>\r\n");
for(auto item:i){
	if(item.count==0)
		continue;
	str.append("<Event>\r\n");

	str.append("<Timestamp>");
	str.append(std::to_string( item.time));
	str.append("</Timestamp>\r\n");

	str.append("<URL>");
	str.append(item.host);
	str.append("</URL>\r\n");

	str.append("<Count>");
	str.append(std::to_string( item.count));
	str.append("</Count>\r\n");
	str.append("</Event>\r\n");
}
str.append("</Events>\r\n");
return str;
}
void *ClientThread(void* Atr)
{
//copy socket
unsigned int myClient_s;
// Input buffer for GET resquest
char in_buf[BUF_SIZE];
std::vector<URLItem> LocalCopy;
std::vector<URLItem> DiffData;
//pthread_mutex_lock(&storageMutex);
//CurentData=data->getAll();
//pthread_mutex_unlock(&storageMutex);
//LocalCopy=CurrentData;


myClient_s = *(unsigned int *)Atr;
unsigned int ReturnCode;
std::string request;
bool IsNonZeroCount= false;
std::string xml;

while (in_buf!="close")
{
	memset(in_buf,'\0',BUF_SIZE);
	ReturnCode = recv(myClient_s, in_buf, BUF_SIZE, 0);
	request=std::string(in_buf);
	if((request=="Get event\r\n")||(request=="Get event"))
	{
		DiffData.clear();
		IsNonZeroCount= false;
		pthread_mutex_lock(&storageMutex);
		DiffData=data->Diff(LocalCopy);
		LocalCopy=data->getAll();
		pthread_mutex_unlock(&storageMutex);
		for(auto item:DiffData)
		{
			if(item.count>0)
				IsNonZeroCount=true;
		}
		if(IsNonZeroCount==false)
			xml="No event\r\n";
		else
			xml= TcpServer::FormatXML(DiffData);
		const char* msg=xml.c_str();
		send(myClient_s, msg, strlen(msg), 0);


	}


}
close(myClient_s);
}

TcpServer::TcpServer()
{

}

TcpServer::~TcpServer() {
	// TODO Auto-generated destructor stub
	close(this->_ServerS);
}


/*
 * HostStorage.cpp
 *
 *  Created on: 11 дек. 2013 г.
 *      Author: root
 */

#include <HostStorage.h>
/*
 * HostStorage();
	virtual ~HostStorage();
	URLItem getItem(std::string url);
	void addItem(std::string url);
	std::vector<URLItem> getAll();
 */
HostStorage::HostStorage()
{

}
/*
 * std::string host;
int count;
time_t time;
};
 */
HostStorage::~HostStorage() {
_data.clear();
}
void HostStorage::addItem(std::string url){
	bool f=false;
	for(auto &i:this->_data){
		if(i.host==url){
			i.count++;
			i.time=time(NULL);
			f=true;
		}

	}
	if(f==false)
	{
		URLItem t;
		t.host=url;
		t.count=1;
		t.time=time(NULL);
		this->_data.push_back(t);
	}

}
std::vector<URLItem> HostStorage::Diff(std::vector<URLItem> Items){
	if(Items.size()==0)
		return this->getAll();
std::vector<URLItem> t;
URLItem diff;
for(URLItem j: Items)
{
	for(URLItem i: this->_data){
		if(i.host==j.host)
		{
			diff.host=i.host;
			diff.count=i.count-j.count;
			diff.time=i.time;
			t.push_back(diff);
			break;
		}
	}

}
bool f=false;
for(URLItem j:this->_data ){
	f=false;
	for(auto i:Items)
	{
		if(i.host==j.host)
			f=true;
	}
	if(f==false)
		t.push_back(j);
}
return t;
}

URLItem HostStorage::getItem(std::string url)
{URLItem t;
t.host="";
t.count=0;
t.time=time(NULL);
for(auto &i:this->_data){
	if(i.host==url)
		t=i;
}
return t;
}
std::vector<URLItem> HostStorage::getAll()
{
	auto t=std::vector<URLItem>(this->_data);
	return t;
}


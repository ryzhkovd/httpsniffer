/*
 * HostStorage.h
 *
 *  Created on: 11 дек. 2013 г.
 *      Author: root
 */
#ifndef HOSTSTORAGE_H_
#define HOSTSTORAGE_H_
#include <queue>
#include <string>
#include <vector>
#include <sys/time.h>
struct URLItem
{
std::string host;
int count;
time_t time;
};
class HostStorage {
public:
	HostStorage();
	virtual ~HostStorage();
	URLItem getItem(std::string url);
	void addItem(std::string url);
	std::vector<URLItem> getAll();
	std::vector<URLItem> Diff(std::vector<URLItem>);

private:
std::vector<URLItem> _data;
};

#endif /* HOSTSTORAGE_H_ */

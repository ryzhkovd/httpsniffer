/*
 * PacketParser.h
 *
 *  Created on: Dec 9, 2013
 *      Author: root
 */


#ifndef PACKETPARSER_H_
#define PACKETPARSER_H_
#include <string>
//#include "headers.h"

class PacketParser {
public:
	PacketParser(std::string data);
	PacketParser(char* playload, size_t len);
	virtual ~PacketParser();
	bool parser();
	std::string getHost();

private:
std::string _data;
std::string _host;
};

#endif /* PACKETPARSER_H_ */
